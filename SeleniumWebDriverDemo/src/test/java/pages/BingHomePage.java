package pages;

import io.opentelemetry.trace.attributes.SemanticAttributes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BingHomePage {
    private WebDriver driver;

    public BingHomePage(WebDriver webDriver){
        driver = webDriver;
    }

    public WebElement getSearchInput(){
        return driver.findElement(By.name("q"));
    }

    public WebElement getSearchButton(){
        return driver.findElement(By.xpath("//label[@for='sb_form_go']"));
    }

    public WebElement getLogo(){
        return driver.findElement(By.xpath("//h1[@class='logo_cont']"));
    }

    public void searchTermAndSubmit(String term){
        getSearchInput().sendKeys(term);
        getSearchButton().click();
    }
}
